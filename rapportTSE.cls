\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{rapportTSE}[09/08/2019, alpha]

% Modèle maintenu sur:
% https://gitlab.com/JChataigne/tse-modele-rapport-de-stage-latex

% ***** Crédits *****
% basé sur: https://github.com/jp-um/university_of_malta_LaTeX_dissertation_template, disponible sous license Creative Commons CC BY 4.0
% placer l'image de fond de la page de titre: http://tug.org/pracjourn/2008-1/mori/mori.pdf
% ********************

% La compilation fonctionne sur Overleaf avec le compilateur pdfLaTeX
% Il y a une erreur de compilation autour de la ligne 229, du à l'ajout du titre "Table des Matières". Elle ne semble pas avoir d'incidence sur le document.
%


% to do list:
% - numéros de page et confidentialité (en page de titre) à étendre jusqu'au bord

% Improvements:
% - page remerciements (optionnelle) https://en.wikibooks.org/wiki/LaTeX/Document_Structure
% - bibliographie
% - titre alternatif pour les en-têtes
% - rapport de taille cadre/numéros de page
% - fichier readme


\LoadClass[11pt,a4paper,twoside french,final,openany]{memoir}


%% **************** Packages (Début) *********************

\RequirePackage[utf8]{inputenc}      % Required for inputting international characters
\RequirePackage[T1]{fontenc}         % Output font encoding for international characters
\RequirePackage{xcolor}              % For named colors
\RequirePackage{eso-pic}             % required to place huge uni logo at the back - on title page
\RequirePackage[pdftex]{graphicx}    % For pictures
\RequirePackage[pdfusetitle]{hyperref}             % For hyperreferences
\RequirePackage[francais]{babel}     % Load babel if you're unsure about the default language - mostly to be safe
\RequirePackage{geometry}
\RequirePackage{fancybox}
\RequirePackage{titletoc}
\RequirePackage{tikz}
\RequirePackage[defaultsans]{lato}		 % Sans font to use


%% ****************** Packages (Fin) *********************




%% ***************** Définitions (Début) *****************

% couleurs
\definecolor{TSEbleu}{HTML}{1f497d}
\definecolor{TSEvert}{HTML}{4f6228}
\definecolor{TSErouge}{HTML}{c0504d}
\definecolor{TSEgris}{HTML}{444444}

% éléments variables
\def\subtitle#1{\gdef\@subtitle{#1}}
\def\confidentialite#1{\gdef\@confidentialite{#1}}
\def\datesstage#1{\gdef\@datesstage{#1}}
\def\tuteurtse#1{\gdef\@tuteurtse{#1}}
\def\tuteurentreprise#1{\gdef\@tuteurentreprise{#1}}
\def\entreprise#1{\gdef\@entreprise{#1}}
\def\typestage#1{\gdef\@typestage{#1}}
\def\annee#1{\gdef\@annee{#1}}
\def\resumeFR#1{\gdef\@resumeFR{#1}}
\def\resumeEN#1{\gdef\@resumeEN{#1}}
\def\motscles#1{\gdef\@motscles{#1}}
\def\keywords#1{\gdef\@keywords{#1}}

%% ***************** Définitions (Fin) *****************







%% ************ Document Options (Début) *****************

\OnehalfSpacing                         % Espacement de lignes de une et demie
\setlength{\headsep}{1.5cm}             % Espace entre l'en-tête et le texte
\nouppercaseheads						% Ne pas convertir les titres en capitales
\setlrmarginsandblock{2.0cm}{1.5cm}{*}  % Marges
\checkandfixthelayout                   % Mettre le layout en effet
\graphicspath{{./images/}}              % Chemin des images
\DeclareGraphicsExtensions{.pdf,.jpeg,.png,.jpg}    % extensions graphiques à charger

% ***** Style des titres de sections et chapitres (début) *****
\chapterstyle{TSEchapter}               % style des chapitres
\setsecheadstyle{\sffamily\LARGE\bfseries\color{TSEvert}}   % Style des \section
\setsubsecheadstyle{\sffamily\Large\color{TSEbleu}}         % Style des \subsection
\setsecnumdepth{subsection}             % ajouter le numéro des sous-sections
\setsecnumformat{\csname the#1\endcsname\enskip\enskip}     % Style sections and subsections
% ***** Style des titres de sections et chapitres (fin) *****

%% ************* Document Options (Fin) ******************






%% ************* En-tête et pied de page (Début) ******************

% Définir le style de page 'TSEpagestyle'
\makepagestyle{TSEpagestyle}
% Il faut définir deux fois les en-têtes et pieds de pages (pour les pages paires et impaires)
\makeevenhead{TSEpagestyle}
    {\includegraphics[height=1.2cm]{logoTSEheader}}
    {\Large\sffamily\color{TSEgris} \textbf{\@title} \vfill }
    {\includegraphics[height=1cm]{images/imt.png}\hspace{1mm}\includegraphics[height=1cm]{images/ujm.png}}
\makeoddhead{TSEpagestyle}
    {\includegraphics[height=1.2cm]{logoTSEheader}}
    {\Large\sffamily\color{TSEgris} \textbf{\@title} \vfill }
    {\includegraphics[height=1cm]{images/imt.png}\hspace{1mm}        \includegraphics[height=1cm]{images/ujm.png}}
\makeevenfoot{TSEpagestyle}{}
    {\vfill \Large\sffamily\color{TSEgris} \emph{ \@author \\ \@annee}}
    {\colorbox{TSErouge}{\hspace{0.3cm}{\color{white} \LARGE\sffamily \thepage }\hspace{0.3cm}}}
\makeoddfoot{TSEpagestyle}{}
    {\vfill \Large\sffamily\color{TSEgris} \emph{ \@author \\ \@annee}}
    {\colorbox{TSErouge}{\hspace{0.3cm}{\color{white} \LARGE\sffamily \thepage }\hspace{0.3cm}}}

%% ************* En-tête et pied de page (Fin) ******************





%% *************** Page de titre (Début) ******************

% Commande pour définir l'image de fond
\newcommand\AlCentroPagina[1]{\AddToShipoutPicture*{\AtPageCenter{\makebox(-300,-300){\includegraphics[width=1\paperwidth]{#1}}}}}

\renewcommand{\maketitle}{

\begingroup
    % Définition marge et famille de police
    \newgeometry{top=0.3in,bottom=0.2in,right=0.5in,left=0.2in}
    \sffamily
    
    \AlCentroPagina{fondTSEpagetitre} % image de fond
    
    % Logo TSE et confidentialité
    \begin{minipage}{0.45\textwidth}
        \includegraphics[width=50mm]{logoTSEpagetitre}
    \end{minipage} %
    \begin{minipage}{0.45\textwidth}
        \raggedleft
        \colorbox{TSErouge}{
            \vphantom{\parbox[c]{1cm}{\rule{1cm}{2cm}}}
            \hspace{0.5cm}
            {\color{white}\large Niveau de confidentialité: \@confidentialite }
            \hspace{0.5cm}}
    \end{minipage}
    
    % Boîte de titre
    \vspace*{2cm} 
    \centering
    \colorbox{TSErouge}{
        \begin{minipage}{0.8\textwidth}
            \centering
            \vspace*{2cm}
            {\HUGE\bfseries{\begin{Spacing}{1.5}\textcolor{white}{\@title}\end{Spacing}}}
            \@ifundefined{@subtitle}{}{\vspace*{3ex}\large\textcolor{white}{\@subtitle}}
            \vspace*{2cm}
        \end{minipage}}
    \vfill
    
    % Informations: auteur, entreprise...
    \begin{flushright}
        \color{TSErouge}
        {\LARGE\bfseries \textsc{\@author}} \\[1.5cm]
        
    	\begin{center}
            \fontsize{200pt}{40pt}
    	    \raggedleft\color{TSErouge}
            \begin{tabular}{ r l }
                {\Large Type de stage:      } & {\Large \@typestage}            \\[0.35cm]
                {\Large Entreprise:         } & {\Large \@entreprise}           \\[0.35cm]
                {\Large Tuteur entreprise:  } & {\Large \@tuteurentreprise}     \\[0.35cm]
                {\Large Tuteur Télécom Saint-Étienne:} & {\Large \@tuteurtse}   \\[0.35cm]
                {\Large Dates de stage:     } & {\Large \@datesstage}           \\[0.35cm]
            \end{tabular}
        \end{center}
        
        % pour ajouter des infos optionnelles
        %\@ifundefined{@cosupervisor}{\\[2ex]}{\\[2ex]\large Co-supervised by \@cosupervisor}\\[1cm]
        %[\baselineskip]
        \vspace*{5ex} 
        {\LARGE \@annee}
    \end{flushright}
    
    
    % logos IMT, UJM et ITII
    \begin{minipage}{\dimexpr\textwidth-1mm}
		\raisebox{-0.575\height} {\includegraphics[height=1.8cm]{imt}}%
		\hspace  { 0.3cm}
		\raisebox{-0.5\height}   {\includegraphics[height=1.8cm]{ujm}}%
		\hspace  { 0.3cm}
		\raisebox{-0.575\height} {\includegraphics[height=2.0cm]{itii}}%
		\hfill%
	\end{minipage}
	
    \thispagestyle{empty}   % pour ne page avoir d'en-tête et pied de page
    \cleardoublepage        % page blanche suivant la page de titre
\endgroup

% Définition des marges, styles de police et de pages pour APRÈS la page de titre
\newgeometry{top=30mm,bottom=30mm,right=25mm,left=25mm}
\sffamily
\pagestyle{TSEpagestyle}
    
}
%% *************** Page de Titre (Fin) ******************



%% *************** Table des Matières (Début) ******************

% change ToC page title style
\renewcommand\tableofcontents{%
    \vspace*{3mm}%
    {\centering\LARGE Table des matières}\par%
    \vspace*{3mm}%
    \@starttoc{toc}%
}

\settocdepth{subsection} % set ToC depth

% pagestyle of ToC first page
\AtBeginDocument{\addtocontents{toc}{\protect\thispagestyle{TSEpagestyle}}}

% style of chapter titles:
\renewcommand{\cftchapterafterpnum}{\par\smallskip\hrule\par\smallskip}
% style of section titles:
\titlecontents{section}[1.8em]{\smallskip\sffamily\bfseries\normalsize}
{\thecontentslabel.\enspace}{}{\hfill\contentspage}
%style of subsection titles:
\titlecontents{subsection}[4.1em]{\smallskip\sffamily\normalsize}
{\thecontentslabel\enspace}{}{\hfill\contentspage}

%% *************** Table des Matières (Fin) ******************


       

%% *************** Chapter headings (Début) ******************

\makechapterstyle{TSEchapter}{%
  %\chapterstyle{default}
  \setlength{\beforechapskip}{0pt}
  \renewcommand*{\chapnumfont}{\sffamily\HUGE\bfseries\color{TSErouge}}
  \renewcommand*{\chaptitlefont}{\sffamily\HUGE\bfseries\color{TSErouge}}
  %\settowidth{\chapindent}{\chapnumfont 111}
  %\renewcommand*{\chapterheadstart}{\begingroup
    %\vspace*{\beforechapskip}%
    %\begin{adjustwidth}{}{-\chapindent}%
    %\hrulefill %barre horizontale au-dessus du titre
    %\smash{\rule{0.4pt}{15mm}} %barre verticale
    %\end{adjustwidth}\endgroup
  %}
  
  \renewcommand*{\printchaptername}{}
  \renewcommand*{\chapternamenum}{}

  \renewcommand*{\printchapternum}{}%
    %\begin{adjustwidth}{}{-\chapindent}
    %\hfill
    %\raisebox{10mm}[0pt][0pt]{\chapnumfont \thechapter}%
    %                          \hspace*{1em}
    %\end{adjustwidth}\vspace*{-3.0\onelineskip}}

  \renewcommand*{\printchaptertitle}[1]{%
    %\vskip\onelineskip
    \thispagestyle{TSEpagestyle}
        \chapnumfont \thechapter. \chaptitlefont ##1
        \par\nobreak
  }
}

\chapterstyle{TSEchapter}

%% *************** Chapter headings (Fin) ******************



%% *************** Page résumés (Début) ******************

\newcommand{\makeabstractpage}{
    \newpage
    \centering
    
    \vspace*{5ex}
    
    \colorbox{TSErouge}{
        \begin{minipage}{1\textwidth}
            \vspace*{2ex}
            {\LARGE\begin{Spacing}{1.5}\textcolor{white}{\hspace{1.5ex}Résumé}\end{Spacing}}
            \vspace*{0.5cm}
            \centering
            \colorbox{white}{
                \begin{minipage}{0.95\textwidth}
                    \vspace{1ex}
                    \large 
                    \@resumeFR \\
                    \vfill
                    \textbf{Mots-clés:} \newline \@motscles
                    \vspace{2mm}
                \end{minipage}
            }
            \vspace*{5pt}
        \end{minipage}
    }
    
    \vspace*{5ex}
    
    \colorbox{TSErouge}{
        \begin{minipage}{1\textwidth}
            \vspace*{2ex}
            {\LARGE\begin{Spacing}{1.5}\textcolor{white}{\hspace{1.5ex}Abstract}\end{Spacing}}
            \vspace*{0.5cm}
            \centering
            \colorbox{white}{
                \begin{minipage}{0.95\textwidth}
                    \vspace{1ex}
                    \large 
                    \@resumeEN \\
                    \vfill
                    \textbf{Keywords:} \newline \@keywords
                    \vspace{2mm}
                \end{minipage}
            }
            \vspace*{5pt}
        \end{minipage}
    }
    
    %\vspace*{5ex}
    
}

%% *************** Page résumés (Fin) ******************









